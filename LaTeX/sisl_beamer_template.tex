\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\usepackage{subfig}
\usepackage[separate-uncertainty=true,detect-mode=true]{siunitx}
\usepackage{amsmath}
\usepackage{amssymb}     % for mathbb
\usepackage{bm} % for bold math
\usepackage{cancel}
\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{enumerate}
\usepackage{xfrac}
\usepackage{float}
\usepackage{booktabs}    % for beautiful tables
\usepackage{lmodern}     % for tiny fonts
\usepackage{sourcesanspro}
\usepackage{listings}
\usepackage[absolute,overlay]{textpos}
\usepackage{fontspec}
\usepackage{array}
\usepackage{changepage}
\usepackage{multirow}

\pgfplotsset{compat=1.11}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{bayesnet}
\usetikzlibrary{fit}
\usetikzlibrary{graphdrawing}
\usetikzlibrary{snakes}
\usetikzlibrary{positioning}
\usetikzlibrary{intersections}
\usetikzlibrary{graphdrawing}
\usetikzlibrary{graphs}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathreplacing}
\usegdlibrary{layered}
\usepgfplotslibrary{groupplots}

\pgfplotsset{compat=1.11,
  /pgfplots/ybar legend/.style={
    /pgfplots/legend image code/.code={%
       \draw[##1,/tikz/.cd,yshift=-0.25em]
        (0cm,0cm) rectangle (3pt,0.8em);},
   },
  ylabsh/.style={ % for aligning y labels in group plots
    every axis y label/.style={
      at={(0,0.5)}, xshift=#1, rotate=90
      }
  },
  jitter/.style={
      x filter/.code={\pgfmathparse{\pgfmathresult+rnd*#1}}
  },
  jitter/.default=0.1
}

\sisetup{group-separator = {,}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math Commands

\newcommand{\vect}[1]{\boldsymbol{\mathbf{#1}}}
\newcommand{\mat}[1]{\vect{#1}}
\newcommand{\bigo}{O}
\newcommand{\bigomega}{\Omega}
\newcommand{\bigtheta}{\Theta}
\newcommand{\NP}{\mathcal{NP}}
\newcommand{\paren}[1]{\left(#1\right)}
\newcommand{\brock}[1]{\left[#1\right]}
\newcommand{\anglebrackets}[1]{\langle #1 \rangle}
\newcommand{\curly}[1]{\left\{#1\right\}}
\newcommand{\ceil}[1]{\lceil #1 \rceil}
\newcommand{\floor}[1]{\lfloor #1 \rfloor}

\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\subjectto}{\operatornamewithlimits{subject\enspace to}}
\newcommand{\for}{\text{for} \;}
\DeclareMathOperator{\nil}{\textsc{nil}}
\DeclareMathOperator{\Expectation}{\mathbb{E}}
\DeclareMathOperator{\Normal}{\mathcal{N}}
\DeclareMathOperator{\Uniform}{\mathcal{U}}
\newcommand{\minimize}{\operatornamewithlimits{minimize}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Review Commands

\newcommand{\todo}[1]{\textcolor{magenta}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Usage: include an image whose corners you want to be filleted
% ex:
%     \begin{tikzpicture}
%        \includeroundedimage{width=0.6\textwidth}{figures/myfig}
%     \end{tikzpicture}
%
\newcommand{\includeroundedimage}[2]{%
\node [inner sep=0pt] (img) at (0,0) {\includegraphics[#1]{#2}};
\fill [background] ($(img.north west)+(0,-0.5cm)$) arc (180:90:0.5cm)  -- ++(0,0.05cm)  -- ++(-0.55cm,0)  -- ++(0,-0.55cm) -- cycle;
\fill [background] ($(img.north east)+(-0.5cm,0)$) arc (90:0:0.5cm)    -- ++(0.05cm,0)  -- ++(0,0.55cm)   -- ++(-0.55cm,0) -- cycle;
\fill [background] ($(img.south east)+(-0.5cm,0)$) arc (-90:0:0.5cm)   -- ++(0.05cm,0)  -- ++(0,-0.55cm)  -- ++(-0.55cm,0) --cycle;
\fill [background] ($(img.south west)+(0,0.5cm)$)  arc (180:270:0.5cm) -- ++(0,-0.05cm) -- ++(-0.55cm,0)  -- ++(0,0.55cm) --cycle;
\draw [background, rounded corners=0.5cm, line width=0.05cm] (img.north west) -- (img.north east) -- (img.south east) -- (img.south west) -- cycle;
}

% Usage:
%   Create a dividing slide (with a different look) that has the given title
\newcommand{\sectionslide}[1]{%
  \begin{frame}[c]{}
    \begin{textblock*}{10cm}(0cm,0cm)
      \tikz {
        \fill[cardinal] (0,0) -- (9cm,0) -- (13cm,4cm) -- (13cm,9cm) -- (0cm,9cm) -- cycle;
        \node[white, anchor=west] at (1.5cm,4.5cm) {\Huge \usebeamerfont{title} #1};
      }
    \end{textblock*}
  \end{frame}
}

% Usage:
%  Create a slide which only contains an image maximized to fit the slide
%  Note - latex must be run twice for it to set correctly.
\newcommand{\onlyimage}[1]{%
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture,overlay]
      \node[at=(current page.center)] {
        \includegraphics[width=\paperwidth]{#1}
      };
    \end{tikzpicture}
  \end{frame}
}


\setbeameroption{hide notes}
\setbeamertemplate{note page}[plain]
% \setbeamercovered{transparent} % show grayed out <paused> items

\usetheme{default}
\useinnertheme{circles}
\beamertemplatenavigationsymbolsempty
\hypersetup{pdfpagemode=UseNone} % don't show bookmarks on initial view
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{enumerate items}[default]

\usefonttheme{professionalfonts}
\setmainfont{Source Sans Pro}
\setmonofont{Source Code Pro}
\setbeamerfont{title}{series={\bfseries \scshape},size=\Huge,parent=structure}
\setbeamerfont{subtitle}{series={\mdseries \scshape},size=\Large,parent=structure}
\setbeamerfont{frametitle}{series=\scshape}
\setbeamerfont{note page}{family*=pplx,size=\footnotesize} % Palatino for notes
\setbeamerfont{author}{series=\mdseries,size=\normalsize,parent=structure}

\definecolor{foreground}{RGB}{0,0,0}
\definecolor{background}{RGB}{255,255,255}
\definecolor{title}{RGB}{0,0,0}
\definecolor{gray}{RGB}{89,89,89}
\definecolor{bluish}{RGB}{101,161,216}
\definecolor{greenish}{RGB}{146,208,80}
\definecolor{cardinal}{RGB}{140,21,21}
\definecolor{TUeRed}{RGB}{170,0,0}

\definecolor{colorBluish}{RGB}{101,161,216}
\definecolor{colorBluePastel}{RGB}{139,185,226}
\definecolor{colorGreenish}{RGB}{106,130,94}
\definecolor{colorGreenPastel}{RGB}{150,194,154}
\definecolor{colorRedish}{RGB}{140,21,21}
\definecolor{colorRedPastel}{RGB}{164,117,129}
\definecolor{colorOrangish}{RGB}{237,125,49}
\definecolor{colorOrangePastel}{RGB}{213,169,143}

\setbeamercolor{subtitle}{fg=cardinal}
\setbeamercolor{author}{fg=gray}
\setbeamercolor{institute}{fg=gray}
\setbeamercolor{normal text}{fg=foreground,bg=background}
\setbeamercolor{frametitle}{bg=cardinal}
\setbeamercolor{frametitle}{fg=background}

\setbeamercolor{item}{fg=foreground} % color of bullets
\setbeamercolor{subitem}{fg=foreground}
\setbeamercolor{itemize/enumerate subbody}{fg=foreground}
\setbeamertemplate{itemize subitem}{{\textendash}}
\setbeamerfont{itemize/enumerate subbody}{size=\footnotesize}
\setbeamerfont{itemize/enumerate subitem}{size=\footnotesize}

% Latin Modern Mono
\newcommand{\latinModernMono}{\fontfamily{lmtt}\selectfont}

\setbeamertemplate{footline}{
    \raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}
          \scriptsize\insertframenumber}}}\hspace*{5pt}
}

\addtobeamertemplate{frametitle}{\vskip-0.5ex}{
  \begin{textblock*}{100mm}(1.05\textwidth,0cm)
    \tikz {
      \fill[background] (0,0) -- (2cm,0) -- (2cm,1.1cm) -- (0.5cm,1.1cm) -- cycle;
      \node[foreground] at (0.8cm,0.65cm) {\large\insertframenumber};
    }
  \end{textblock*}
}

\tikzset{
  every overlay node/.style={
    draw=none,fill=white,rounded corners,anchor=north west,
  },
  invisible/.style={opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
  },
}

\def\tikzoverlay{%
   \tikz[baseline,overlay]\node[every overlay node]
}%

\defbeamertemplate*{title page}{customized}[1][]
{
  \vspace{3cm}
  \usebeamerfont{title}\inserttitle\par
  \usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par
  \vspace{0.75cm}
  \usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor\par
  \usebeamerfont{author}\usebeamercolor[fg]{author}\insertdate\par
}

% text that wraps around, is raggedright and allows \newline manual line break
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% text that wraps around, is centered and allows \newline manual line breaks
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% text that wraps around, is raggedleft and allows \newline manual line breaks
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}